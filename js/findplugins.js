// @licstart  The following is the entire license notice for the
//  JavaScript code in this page.
//
//  Copyright (C) 2010, 2011  Ivaylo Valkov <ivaylo@e-valkov.org>
//
//  The JavaScript code in this page is free software: you can
//  redistribute it and/or modify it under the terms of the GNU
//  General Public License (GNU GPL) as published by the Free Software
//  Foundation, either version 3 of the License, or (at your option)
//  any later version.  The code is distributed WITHOUT ANY WARRANTY
//  without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
//
//  As additional permission under GNU GPL version 3 section 7, you
//  may distribute non-source (e.g., minimized or compacted) forms of
//  that code without the copy of the GNU GPL normally required by
//  section 4, provided you include this license notice and a URL
//  through which recipients can access the Corresponding Source.
//
//  @licend  The above is the entire license notice
//  for the JavaScript code in this page.
//
// @source http://e-valkov.org/linterna-magica/js/findplugins.js

find_plugins();

function find_plugins()
{
    var string ;
    var dl;
    var output = document.getElementById("detected_plugins");
    var mime_flv =  navigator.mimeTypes["video/flv"]
    var mime_mp4 = navigator.mimeTypes["video/mp4"];
    var mime_flash = navigator.mimeTypes["application/x-shockwave-flash"];
    dl = document.createElement("dl");
    var array = new Array(mime_flv, mime_mp4, mime_flash);

    if (mime_flv || mime_mp4 || mime_flash)
    {
	var msg = document.getElementById("noplugins");
	msg.parentNode.removeChild(msg);
    }

    for (var i=0; i<array.length; i++)
    {
	p = array[i];

	if (p)
	{
	    var dt = document.createElement("dt");
	    var dd = document.createElement("dd");
	    var t = document.createTextNode(p.description + " ("+p.type+")");
	    var d = document.createTextNode("Plugin not enabled?");

	    dt.appendChild(t);
	    dl.appendChild(dt)

	    if (p.enabledPlugin)
	    {
		var dl2 = document.createElement("dl");
		var dt2 = document.createElement("dt");
		var dd2 = document.createElement("dd");
	    
		var t2 = document.createTextNode(p.enabledPlugin.name);
		var d2 = document.createTextNode(p.enabledPlugin.description);


		dt2.appendChild(t2);
		dd2.appendChild(d2);
		dl2.appendChild(dt2);
		dl2.appendChild(dd2);
		dd.appendChild(dl2);
	    }
	    else
	    {
		dd.appendChild(d);
	    }

	    dl.appendChild(dd)
	}
    }

    output.appendChild(dl);
}