// @licstart  The following is the entire license notice for the
//  JavaScript code in this page.
//
//  Copyright (C) 2010  Anton Katsarov <anton@katsarov.org>
//  Copyright (C) 2010, 2011, 2014 Ivaylo Valkov <ivaylo@e-valkov.org>
//
//  The JavaScript code in this page is free software: you can
//  redistribute it and/or modify it under the terms of the GNU
//  General Public License (GNU GPL) as published by the Free Software
//  Foundation, either version 3 of the License, or (at your option)
//  any later version.  The code is distributed WITHOUT ANY WARRANTY
//  without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
//
//  As additional permission under GNU GPL version 3 section 7, you
//  may distribute non-source (e.g., minimized or compacted) forms of
//  that code without the copy of the GNU GPL normally required by
//  section 4, provided you include this license notice and a URL
//  through which recipients can access the Corresponding Source.
//
//  @licend  The above is the entire license notice
//  for the JavaScript code in this page.
//
// @source http://linternamagica.org/js/page.js

var acc;
var winScroll;
var clicked;
var focusId;
window.addEvent('domready', function () {

    // Piwik tracking
    // Destroy the static tracker
    try {
	$('piwik-image-tracker').destroy();
    } catch (err) {}

    var pkBaseURL = (("https:" == document.location.protocol) ?
		     "https://piwik.e-valkov.org/piwik/" :
		     "http://piwik.e-valkov.org/piwik/");
    var piwik_site_id = 3;

    var script = document.createElement("script");
    script.setAttribute('src', pkBaseURL+"piwik.js");
    script.setAttribute('defer', 'defer');
    script.setAttribute('async', 'async');
 
    $(script).inject($$('head')[0], "bottom");

    window.piwikTracker = null;
    window.piwik_timer = 0;
    var piwik_ticker = function() {
 	try {
	    window.piwik_timer++;
	    window.piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", piwik_site_id);
	    var piwikTracker = window.piwikTracker;
	    var extensions = "user.js.html|user.js|user.js.gz|"+
		"user.js.bz2|user.js.xz|sig|gz|xz|bz2|tar.gz|"+
		"tar.xz|tar.bz2";
	    piwikTracker.addDownloadExtensions(extensions);
	    piwikTracker.trackPageView();
	    piwikTracker.enableLinkTracking();
	    clearInterval(window.piwik_ticker_id);
	} catch(err) {}
	
	if (window.piwik_timer > 10) {
	    clearInterval(window.piwik_ticker_id);
	}
    }
    
    window.piwik_ticker_id = piwik_ticker.periodical(1000);
    
    // End of Piwik tracking

	//new Fx.SmoothScroll(window);
	acc = new Fx.Accordion($$('h2'), $$('.hider'),{
		display: -1,
		alwaysHide: true,
		trigger: 'click',
		initialDisplayFx: false,
		onComplete: function () {
		    scrollToTarget();
		}

	});

	// Remove the single image static header gallery
	// Clone the screenshots and insert them as the new gallery
	var gallery = $("gallery-static").clone();
	gallery.setProperty("id","gallery-clone");
	gallery.inject("gallery", "before");
	$("gallery").destroy();
	$("gallery-clone").setProperty("id", "gallery");

	new viewer($$('#gallery a'),{
		mode: 'alpha',
		    interval: 5000
		    }).play(true);


    // Slimbox had processed the page before the dynamic gallery was created
    // If the scanPage methos is used again we wil have duplicate of each image.
    // We stop the default processing of the click, and fire a click event on the 
    // <a> element form the static-gallery with the same index (same image).
    // These elements are processed by Slimbox, so we have a viewer ;)
    // $$('#gallery a').each(function(el, index) {
    // 	el.addEvent('click', function(e){
    // 	    new Event(e).stop();
    // 	    $$('#gallery-static a')[index].fireEvent('click');
    // 	});
    // });

	$$('a[href^=#]').each(function (el) {
		el.addEvent('click', function (e) {
			new Event(e).stop();
			focusId = el.get('href').split("#")[1];
			goToElement();
			//clicked = 'a';
		    });
	    });
	winScroll = new Fx.Scroll(window, {
		onComplete: function () {
		    document.location.href = document.location.href.split("#")[0] + "#" + focusId;
		}
	    });

    // // What language localisation to use for  Chronology
    // var locale = "en";

    // // Try to force the locale to Bulgarian for the bulgarian translation
    // if (/index\.bg\.html/i.test(window.location.href))
    // {
    // 	locale = "bg";
    // }

    // var chronology_options = 
    // 	{
    // 	    log_to:"web",
    // 	    group_name: "linternamagica",
    // 	    debug: true,
    // 	    mini_logo: false,
    // 	    local_testing: true,
    // 	    parent_node: "identica-content",
    // 	    count:10,
    // 	    protocol:'https://',
    // 	    locale: locale,
    // 	    css: {
    // 		styles: 
    // 		{
    // 		    chronology_container: "width: inherit !important;"
    // 		    // chronology_container: "display: block; position: relative;",
    // 		    // chronology_download_anchor: "display: block; position: absolute; top:0 ; right: 0;",
    // 		    // notice_li: "margin-bottom: 8px;"
    // 		}
    // 	    }
    // 	};

    // var chronology = new Chronology(chronology_options);

});

window.addEvent('load', function () {
	var id =  window.location.href.split('#')[1];
	if (id) focusId = id;
	goToElement();
	$$('h2').each(function (h2) {
		h2.addEvent('click', function () {
			focusId =  h2.get('id');
		    });
	    });
    });


var goToElement = function() {
    // #config
    if (focusId) {
	var id = focusId;
	var index;
	var is_visible;
	if ($(id)) {
	    index = $$('h2').indexOf($(id));
	    if ('h2' == $(id).get('tag')) {
		index = $$('h2').indexOf($(id));
	    } else {

		var position = $(id).getParent('div.hider');
		if (!position)
		    return;

		// If the link is in the same section/div, everything becomes closed
		// So we check if it is vissible 
		is_visible = $(position).getStyle("visibility");

		index = $$('h2').indexOf($(position).getPrevious("h2"));
	    }

	    // See the note for the is_visible variable
	    if (is_visible != "visible") {
		acc.display(index);
	    }
	    else {
		// Scroll manualy because we do not use the accordion now
		scrollToTarget();
	    }
	}
    }
};

var scrollToTarget = function () {
    if (focusId) {
	var id = focusId;
	var dalayedScroll = function() {winScroll.toElement($(id));}
	dalayedScroll.delay(50);
    }
};
