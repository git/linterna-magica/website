<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html
	  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  This HTML file is part of  Linterna Mágica -->

<!--  Copyright (C) 2011, 2012, 2013, 2014 Ivaylo Valkov <ivaylo@e-valkov.org> -->
<!--  Copyright (C) 2011, 2012, 2013 Anton Katsarov <anton@katsarov.org> -->

<!--  Linterna Mágica is free software: you can redistribute it and/or modify -->
<!--  it under the terms of the GNU General Public License as published by -->
<!--  the Free Software Foundation, either version 3 of the License, or -->
<!--  (at your option) any later version. -->

<!--  Linterna Mágica is distributed in the hope that it will be useful, -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the -->
<!--  GNU General Public License for more details. -->

<!--  You should have received a copy of the GNU General Public License -->
<!--  along with Linterna Mágica.  If not, see <http://www.gnu.org/licenses/>. -->

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Downloading Linterna M&aacute;gica <!--#echo var="REDIRECT_LM_DOWNLOAD"--></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <meta name="author" content="Ivaylo Valkov - ivaylo@e-valkov.org" />
    <meta name="designer" content="Anton Katsarov - anton@katsarov.org" />
    
    <link rel="stylesheet" href="../css/screen.css" type="text/css" media="screen" />

    <link rel="icon" type="image/png" href="../css/favicon.png" />
    <link rel="icon" type="image/vnd.microsoft.icon" href="../css/favicon.ico" />

    <script type="text/javascript" src="../js/mootools.js"></script>
    <script type="text/javascript" src="../js/download-page.js"></script>

  </head>
  <body>
    <div class="wrap">
      <div class="content">
	<div class="header">
	  <h1>Linterna M&aacute;gica</h1>
	  <a id="web-install-download" href='sv-mirror/linternamagica-v<!--#echo var="REDIRECT_LM_DOWNLOAD"-->'>
	    Download <span>version <!--#echo var="REDIRECT_LM_VERSION" --></span>
	  </a>
	  <div class="other-downloads-download-page">
	       <a id="direct-link-download-page" href="sv/linternamagica-v<!--#echo var="REDIRECT_LM_DOWNLOAD"-->">direct link</a>

	       <a id="download-area-download-page" href="sv/">download area</a>
	       <a id="download-area-mirror-download-page" href="sv-mirror/">download area (mirror)</a>
	</div>

	  <div class="download-page-donation-notice">
	    <p>Thank you for downloading and using Linterna M&aacute;gica!</p>
	    <p>
	      If you like <a href="http://linternamagica.org">Linterna M&aacute;gica</a> and find it useful,
	      consider making a donation. <a href="http://linternamagica.org#donations">Donatiing</a> will ensure that
	      you will have at least one option to watch videos on the
	      web with a player that respects
	      your <a href="https://www.gnu.org/philosophy/free-sw.html">software
	      freedom</a>.  Thank you!
	    </p>

	    <div id="donate-download-page" >

	      <div class="donation-method-download-page donate-paypal-download-page" >
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
		  <p>
		    <input type="hidden" name="cmd" value="_s-xclick" />
		    <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHNwYJKoZIhvcNAQcEoIIHKDCCByQCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYCejXQZyx6Qqebizjf5utb5fFnOOW9QwrUafBDpM6lYKa6xfpcJDheeYsacPW51kjCLx3Qa8AsTBOyAlzwI3ugxwWwFpnkVX/aj3eUASww6glu9z8fizBmT+Z/odMQezDTiZnIuPEYNwgnEyfbxsx/YY1k/faSvwa9HehAzhZjX2DELMAkGBSsOAwIaBQAwgbQGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI7jSXP6wrS66AgZBy1fgdDELPbR4nAF5pwbm6zflWMeu2o6pDbJKHbJEfwUMs2QvJe2Z2XTnDzxilb0f6rOtXVp2ykZAiDSNjq5/rW+Blih+NaJ29yTzja0alScrCMGxIwONLvaHdzukqiLT20htB2uP6galonUo3SNk7sv8jgOJkO4LHVrkcVusxxvwFFIs9PAFVyiwJORn8FwWgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xMTAzMjAyMTUwMjBaMCMGCSqGSIb3DQEJBDEWBBTquS3KkoO9Ks/PQSGbbf9sR+AbOjANBgkqhkiG9w0BAQEFAASBgIMK3ggNJSNK+O57Qq+yv+axzqygsJz9zDUkf18OJw1tDNnUXubu7a+RWLdKnv+7M50hu28fTrglqG18pxhjqubGE5ieyBmLSAL/yjMtF3iAQx58yJKz1vxpq/DICXsQo2QeK5UvInyuCAC1J0Y1V/43unYXXnBQIHZuumxaZJHP-----END PKCS7-----
								 " />
		    <input type="image" src="https://www.paypalobjects.com/WEBSCR-640-20110306-1/en_GB/i/btn/btn_donate_SM.gif" title="PayPal"  name="submit" alt="PayPal - The safer, easier way to pay online." />
		    <img alt=""  src="https://www.paypalobjects.com/WEBSCR-640-20110306-1/en_US/i/scr/pixel.gif" width="1" height="1" />
		  </p>
		</form>
	      </div>

	    </div><!-- donate -->

	    <p class="clear-donation-download-page">

	      The download process should start
	      automatically 
	      <span class="pay-attention-text">within <span id="ticker">30</span>
		seconds.</span> If it doesn't, or if you have issues with
		mirrors use the links at the right.
	    </p>
	  </div>
	</div>

      </div>

      <div class="footer">
	<!-- <p class="identica"> -->
	<!--   <a href="http://identi.ca/valkov">Follow us on Identi.ca</a> -->
	<!-- </p> -->

	<address>Author: <a href="http://www.e-valkov.org/">Ivaylo Valkov</a>
	  <a href="mailto:Ivaylo Valkov &lt;ivaylo@e-valkov.org&gt;?subject=About Linterna M&aacute;gica">&lt;ivaylo@e-valkov.org&gt;</a>
	</address>
	<address>
	  Design: <a href="http://www.katsarov.org">Anton Katsarov</a>
	  <a href="mailto:Anton Katsarov &lt;anton@katsarov.org&gt;?subject=About Linterna M&aacute;gica">&lt;anton@katsarov.org&gt;</a>
	</address>
	<p>
	  <a href="http://validator.w3.org/check?uri=referer" title="Valid xHTML 1.0 Strict"> <span>XHTML</span></a>

	  <img id="piwik-static-tracker" width="1" height="1"
	       src="http://piwik.e-valkov.org/piwik/piwik.php?idsite=3" 
	       style="border: 0; display: none;" alt="Piwik static tracker" />
	  <a href="http://piwik.e-valkov.org/piwik/index.php?module=CoreAdminHome&amp;action=optOut&amp;language=en">No tracking</a>
	  <a href="http://identi.ca/valkov">Follow us on Identi.ca</a>
	</p>
      </div>
    </div>
  </body> 
</html>
